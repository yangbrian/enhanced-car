package car;

/**
 * Represents all vehicles
 * @author Brian Yang
 * @version 05.04.2013
 */
public abstract class Vehicle implements Comparable {
    
    /** The name of the vehicle */
    private String model;
    /** the year the car was manufactured */
    private int year;
    /** the daily rate of renting the vehicle */
    private double cost;
    /** measures how likely a customer is to rent the vehicle */
    private double appeal;
    
    /**
     * Constructs a new vehicle
     * @param model the name of the vehicle
     * @param year the year the car was manufactured
     * @param cost the daily rate of renting the vehicle
     */
    public Vehicle(String model, int year, double cost) {
        this.model = model;
        this.year = year;
        this.cost = cost;
        this.appeal = 0.0;
    }
    
    /**
     * The name of the vehicle
     * @return the year and model of the vehicle
     */
    public String getName() {
        return year + " " + model;
    }
    
    /**
     * The daily rate of renting the vehicle
     * @return the daily rate of renting the vehicle
     */
    public double getCost() {
        return cost;
    }
    
    /**
     * The year the vehicle was manufactured
     * @return the year the vehicle was manufactured
     */
    public int getYear() {
        return year;
    }
    
    /**
     * Calculates the appeal of the vehicle, which is dependent on the vehicle type
     * @return the appeal of the vehicle
     */
    public abstract double calculateAppeal();
    
    /**
     * Compare this vehicle's appeal to another
     * @param o the vehicle to compare it to
     * @throws ClassCastException the specified object is not a Vehicle
     * @return a negative integer, zero, or a positive integer as this vehicle's appeal is less than, 
     * equal to, or greater than that of the specified vehicle, respectively
     */
    @Override
    public int compareTo(Object o) {
        // The commented line below works quite elegantly, 
        // until you have two appeal values that are very close together.
        // Because of integer rounding, appeal values of 16.1 and 16.2 will return 0.
        // :(
        
        // return (int)(calculateAppeal() - ((Vehicle)o).calculateAppeal());
        
        if (calculateAppeal() < ((Vehicle)o).calculateAppeal())
            return -1;
        else if (calculateAppeal() > ((Vehicle)o).calculateAppeal())
            return 1;
        else
            return 0;
    }
}