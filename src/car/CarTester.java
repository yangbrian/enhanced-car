package car;

import java.util.ArrayList;
 
/**
 * Tests the Car project <br/>
 * I am not responsible for any errors in this tester. Use at your own risk.
 * @author Brian Yang
 * @version 05.05.2013
 */
public class CarTester implements Runnable {
   
    private Thread thread;
   
    /**
     * Creates the tester object
     * Ignore this. Just look at the run() method
     */
    public CarTester() {
        thread = new Thread();
        thread.start();
        run();
    }
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("This is a tester for your Car project created by Brian Yang / Panther.");
        System.out.println("Please make sure you have enabled \"Unlimited Buffering\" if you are using BlueJ.");
        System.out.println();
        System.out.println("Use this tester at your own risk. I am not responsible for any errors.");
        System.out.println("The tester will start in 3 seconds. If you do not agree to the above, then close this immediately.");
       
        // All the test method calls are in the run() method below
        new CarTester();
    }
 
    /**
     * Runs the tester
     */
    @Override
    public void run() {
       
        // Don't worry about understanding this block :) Change 3000 to something less if you want.
        try {
            thread.sleep(3000);
        } catch (InterruptedException ex) {}
       
        System.out.println();
        System.out.println("Ready? Let's start testing!");
 
        // Start time
        long startTime = System.currentTimeMillis();
 
        // Convertibles
        System.out.println();
        System.out.println("PART 1 - CONVERTIBLES");
        System.out.println("--------------------------------------------");
       
        // Create Convertibles
        System.out.println("Creating 5 convertibles.");
        Convertible conv1 = new Convertible("Mazda MX-5 Miata", 2013, 100.00, 20);
        Convertible conv2 = new Convertible("Ford Mustang", 2011, 110.95, 18);
        Convertible conv3 = new Convertible("Nissan 370Z", 2010, 100.10, 10);
        Convertible conv4 = new Convertible("Mini Cooper", 2009, 90.10, 17);
        Convertible conv5 = new Convertible("The Bakesale Convertible", 1995, 133.70, 30);
       
        ArrayList<Convertible> convertibles = new ArrayList<Convertible>();
        convertibles.add(conv1);
        convertibles.add(conv2);
        convertibles.add(conv3);
        convertibles.add(conv4);
        convertibles.add(conv5);
       
        System.out.println("Convertibles successfully created.");
       
        System.out.println();
        System.out.println("Calculating and printing the information of all the convertibles.");
        System.out.println();
        System.out.println("Actual values:");
        for (Convertible c : convertibles)
            System.out.println(c.getName() + " costs " + c.getCost() + " daily and has an appeal value of " + c.calculateAppeal());
        System.out.println();
        System.out.println("Expected values:");
        System.out.println("2013 Mazda MX-5 Miata costs 100.0 daily and has an appeal value of 19.0\n" +
            "2011 Ford Mustang costs 110.95 daily and has an appeal value of 16.872616244411326\n" +
            "2010 Nissan 370Z costs 100.1 daily and has an appeal value of 8.984096870342773\n" +
            "2009 Mini Cooper costs 90.1 daily and has an appeal value of 16.06521957277695\n" +
            "1995 The Bakesale Convertible costs 133.7 daily and has an appeal value of 28.394743666169894");
       
        System.out.println();
        System.out.println("If the actual values above match the expected values, congratulations, your Convertible class should be correct.");
        System.out.println("The tester will continue in 3 seconds");
       
        try {
            thread.sleep(3000);
        } catch (InterruptedException ex) {}
       
       
 
        // Hybrids
        System.out.println();
        System.out.println("PART 2 - HYBRIDS");
        System.out.println("--------------------------------------------");
       
        // Create Hybrids
        System.out.println("Creating 5 hybrids.");
        Hybrid hybrid1 = new Hybrid("Ford Fusion Hybrid", 2013, 100.00, 47);
        Hybrid hybrid2 = new Hybrid("Hyundai Sonata Hybrid", 2011, 110.95, 34);
        Hybrid hybrid3 = new Hybrid("Toyota Prius V", 2013, 100.10, 44);
        Hybrid hybrid4 = new Hybrid("Panther Mobile", 2009, 190.10, 12);
        Hybrid hybrid5 = new Hybrid("The Bakesale Hybrid", 1995, 133.70, 57);
       
        ArrayList<Hybrid> hybrids = new ArrayList<Hybrid>();
        hybrids.add(hybrid1);
        hybrids.add(hybrid2);
        hybrids.add(hybrid3);
        hybrids.add(hybrid4);
        hybrids.add(hybrid5);
       
        System.out.println("Hybrids successfully created.");
       
        System.out.println();
        System.out.println("Calculating and printing the information of all the hybrids.");
        System.out.println();
        System.out.println("Actual values:");
        for (Hybrid h : hybrids)
            System.out.println(h.getName() + " costs " + h.getCost() + " daily and has an appeal value of " + h.calculateAppeal());
        System.out.println();
        System.out.println("Expected values:");
        System.out.println("2013 Ford Fusion Hybrid costs 100.0 daily and has an appeal value of 45.0\n" +
            "2011 Hyundai Sonata Hybrid costs 110.95 daily and has an appeal value of 31.781\n" +
            "2013 Toyota Prius V costs 100.1 daily and has an appeal value of 41.998\n" +
            "2009 Panther Mobile costs 190.1 daily and has an appeal value of 8.198\n" +
            "1995 The Bakesale Hybrid costs 133.7 daily and has an appeal value of 54.326");
       
        System.out.println();
        System.out.println("If the actual values above match the expected values, congratulations, your Hybrid class should be correct.");
        System.out.println("The tester will continue in 3 seconds");
       
        try {
            thread.sleep(3000);
        } catch (InterruptedException ex) {}
       
       
 
        // Car Rental
        System.out.println();
        System.out.println("PART 3 - CAR RENTAL");
        System.out.println("--------------------------------------------");
       
        ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
        vehicles.add(conv1);
        vehicles.add(hybrid2);
        vehicles.add(conv3);
        vehicles.add(conv4);
        vehicles.add(hybrid1);
       
        // Create Car Rental place
        System.out.println("Creating 2 CarRental places, one blank one and another with 5 vehicles.");
        CarRental loser = new CarRental("Bakesale's World of Lame Cars");
        CarRental rental = new CarRental("Bakesale's World of Awesome Cars", vehicles);
        System.out.println("CarRental places successfully created.");
       
        // Return the Lot
        System.out.println();
        System.out.println("Printing the list of car names in " + rental.getName() + ". Should be sorted already.");
       
        System.out.println();
        System.out.println("Actual:");
        System.out.println(rental.getLot());
       
        System.out.println();
        System.out.println("Expected:");
        System.out.println("2013 Ford Fusion Hybrid, 2011 Hyundai Sonata Hybrid, 2013 Mazda MX-5 Miata, 2009 Mini Cooper, 2010 Nissan 370Z");
       
        System.out.println();
        System.out.println("If the actual values above match the expected values, congratulations, your CarRental class is properly sorting everything.");
        System.out.println("I wish I could say your CarRental class is completely correct, but we're not done yet!");
        System.out.println("The tester will continue in 3 seconds");
       
        try {
            thread.sleep(3000);
        } catch (InterruptedException ex) {}
       
       
 
        // Car Rentals 2
        System.out.println();
        System.out.println("PART 4 - ADDING AND RENTING CARS");
        System.out.println("--------------------------------------------");
       
        System.out.println("Adding 2 cars");
        rental.addCar(hybrid5);
        rental.addCar(conv5);
        System.out.println("Renting most appealing car for 30 days.");
        System.out.println("Actual: " + rental.rentOut(30));
        System.out.println("Expected: 4010.9999999999995");
        System.out.println();
       
        System.out.println("Renting the next most appealing car for 20 days.");
        System.out.println("Actual: " + rental.rentOut(20));
        System.out.println("Expected: 2000.0");
        System.out.println();
        System.out.println("Adding 1 more car.");
        rental.addCar(conv2);
       
        System.out.println();
       
        System.out.println("2 cars were added. 2 were rented. 1 more was added.");
        System.out.println("Printing the final list of car names. Should be sorted already.");
       
        System.out.println();
        System.out.println("Actual:");
        System.out.println(rental.getLot());
       
        System.out.println();
        System.out.println("Expected:");
        System.out.println("2011 Hyundai Sonata Hybrid, 1995 The Bakesale Convertible, 2013 Mazda MX-5 Miata, 2011 Ford Mustang, 2009 Mini Cooper, 2010 Nissan 370Z");
       
        System.out.println();
        System.out.println("If the actual values above match the expected values, congratulations, your CarRental class should be correct.");
        System.out.println("Could we be done??");
        System.out.println("Because I like to make you wait in suspense, this tester will continue in 5 seconds!");
       
        try {
            thread.sleep(5000);
        } catch (InterruptedException ex) {}
       
       
 
        // Congrats
        System.out.println();
        System.out.println("CONGRATULATIONS");
        System.out.println("--------------------------------------------");
       
        System.out.println("Congratulations! If the printed Actual values matched the Expected values, then your project should be complete.");
        System.out.println();
        System.out.println("Please note that this tester does not test for elegance or efficiency.");
        System.out.println();
        System.out.println("Make sure your Vehicle class is declared as abstract.");
        System.out.println();
        System.out.println("Make sure the calculateAppeal() method in your Convertible class does not merely use 2013 as the year. You need to make this work for ANY year.");
       
        System.out.println();
        System.out.println("This test took " + (System.currentTimeMillis() - startTime)/1000.0 + " seconds to execute. Anything (14.0, 14.5] seconds is optimal.");
        System.out.println("Thanks for using my tester. Have a great day. Good luck on your AP exams.");
        System.out.println();
        System.out.println("May 5, 2013 - Brian Yang / Panther / Bakesale");
    }
}