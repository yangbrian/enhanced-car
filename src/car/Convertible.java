package car;

import java.util.Calendar;

/**
 * Represents a convertible car
 * @author Brian Yang
 */
public class Convertible extends Vehicle {
    
    /** coolness level */
    private int coolness;
    /** appeal cost factor */
    private double APPEAL_COST_FACTOR = 100.0;
    
    /**
     * Constructs a new convertible
     * @param model the name of the convertible
     * @param year the year the convertible was manufactured
     * @param cost the daily rate of renting the convertible
     * @param coolness how cool the convertible is
     */
    public Convertible(String model, int year, double cost, int coolness) {
        super(model, year, cost);
        this.coolness = coolness;
    }
    
    /**
     * Calculates the appeal of the convertible using the formula: 
     * <code>coolness * (model year / current year) - cost / 100</code>
     * @return the appeal of the convertible
     */
    @Override
    public double calculateAppeal() {
        return coolness * ((getYear()*1.0)/Calendar.getInstance().get(Calendar.YEAR)*1.0) - getCost()/APPEAL_COST_FACTOR;
    }
    
}