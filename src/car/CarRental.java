package car;

import java.util.ArrayList;
// import java.util.Collections;

/**
 * Represents the place that customers rent cars from
 * @author Brian Yang
 * @version 05.04.2013
 */
public class CarRental {
    
    /** name of rental place */
    private String name;
    /** a sorted list of vehicles */
    private ArrayList<Vehicle> vehicles;
    
    /**
     * Constructs a new empty car rental place
     * @param name the name of the place
     */
    public CarRental(String name) {
        this.name = name;
        this.vehicles = new ArrayList<Vehicle>();
    }
    
    /**
     * Constructs a new car rental place with vehicles already in it
     * @param name the name of the place
     * @param vehicles a list of vehicles
     */
    public CarRental(String name, ArrayList<Vehicle> vehicles) {
        this.name = name;
        this.vehicles = vehicles;
        sort();
    }
    
    /**
     * The name of the car rental place
     * @return the name of the car rental place
     */
    public String getName() {
        return name;
    }
    
    /**
     * The list of car names
     * @return a list of car names, separated by commas, and sorted by appeal
     */
    public String getLot() {
        String lot = "";
        boolean first = true;
        for (Vehicle v : vehicles) {
            if (first) {
                lot += v.getName();
                first = !first;
            }
            else
                lot += ", " + v.getName();
        }
        return lot;
    }
    
    /**
     * Sorts the list of vehicles using the rather inefficient selection sort. <br/>
     * Sorting method based on http://www.algolist.net/Algorithms/Sorting/Selection_sort
     */
    public void sort() {
        // Collections.sort(vehicles, Collections.reverseOrder());

        // Max appeal index
        int maxIndex;
        // current Vehicle being compared
        Vehicle tmp;
        
        int n = vehicles.size();
        for (int i = 0; i < n - 1; i++) {
            maxIndex = i;
            for (int j = i + 1; j < n; j++)
            if (vehicles.get(j).compareTo(vehicles.get(maxIndex)) > 0)
                maxIndex = j;
            if (maxIndex != i) {
                tmp = vehicles.get(i);
                vehicles.set(i, vehicles.get(maxIndex));
                vehicles.set(maxIndex, tmp);
            }
        }
    }

    /**
     * Add a vehicle to the car rental place and resort
     * @param v the vehicle to add
     */
    public void addCar(Vehicle v) {
        vehicles.add(v);
        sort();
    }
    
    /**
     * The car with the most appeal has been rented. 
     * Remove the car from the list and calculate the rental cost.
     * @param days the number of days the car is being rented for
     * @return the cost of renting
     */
    public double rentOut(int days) {
        return vehicles.remove(0).getCost() * days;
    }
}