package car;

/**
 * Represents a hybrid car
 * @author Brian Yang
 * @version 05.04.2013
 */
public class Hybrid extends Vehicle {
    
    /** coolness level */
    private double mph;
    /** appeal cost factor */
    private double APPEAL_COST_FACTOR = 50.0;
    
    /**
     * Constructs a new hybrid vehicle
     * @param model the name of the hybrid
     * @param year the year the hybrid was manufactured
     * @param cost the daily rate of renting the hybrid
     * @param mph fuel efficiency of the hybrid
     */
    public Hybrid(String model, int year, double cost, double mph) {
        super(model, year, cost);
        this.mph = mph;
    }
    
    /**
     * Calculates the appeal of the hybrid using the formula: 
     * <code>mpg - cost/50</code>
     * @return the appeal of the hybrid
     */
    @Override
    public double calculateAppeal() {
        return mph - getCost()/APPEAL_COST_FACTOR;
    }
    
}